package com.example.gallery

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_image.view.*

class ImageFragment(private val image: String, private val position: Int) : BaseFragment() {

//    lateinit var itemView: View
    override fun getLayoutResource() = R.layout.fragment_image

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        setImage()
    }


    @SuppressLint("SetTextI18n")
    private fun setImage() {
        Glide.with(this).load(image).into(rootView!!.imageView)
        rootView!!.textView.text = "Position: ${position + 1}"
    }


}