package com.example.gallery

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_image.view.*
import kotlinx.android.synthetic.main.image_layout.view.*

class ViewPagerAdapter2(private val images: ArrayList<String>) : PagerAdapter() {
    override fun isViewFromObject(view: View, itemView: Any): Boolean {
        return view == itemView
    }

    override fun getCount() = images.size

    @SuppressLint("SetTextI18n")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView =
            LayoutInflater.from(container.context).inflate(R.layout.image_layout, container, false)
        container.addView(itemView)

        Glide.with(itemView.context).load(images[position]).into(itemView.imageView2)
        itemView.textView2.text = "Position: ${position + 1}"

        return itemView

    }

    override fun destroyItem(container: ViewGroup, position: Int, itemView: Any) {
        container.removeView(itemView as View)

    }

}