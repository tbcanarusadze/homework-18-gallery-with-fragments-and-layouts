package com.example.gallery

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_image.*
import kotlinx.android.synthetic.main.fragment_image.view.*

class MainActivity : AppCompatActivity() {
    private lateinit var mPager: ViewPager
    private var images = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        mPager = pagerLayout
        mPager.adapter = ViewPagerAdapter(supportFragmentManager, images)
    }


    private fun init(){
        addUrls()
        fragmentsButton.setOnClickListener {
            mPager.adapter = ViewPagerAdapter(supportFragmentManager, images)
        }
        layoutsButton.setOnClickListener {
            mPager.adapter = ViewPagerAdapter2(images)
        }

    }

    private fun addUrls(){
        images.add("https://cdn.eso.org/images/thumb300y/eso1907a.jpg")
        images.add("https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg")
        images.add("https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80")
        images.add("https://cdn.pixabay.com/photo/2015/06/19/21/24/the-road-815297__340.jpg")
        images.add("https://www.imago-images.de/imagoextern/bilder/stockfotos/imago-images-geniale-luftaufnahmen.jpg")
    }

    override fun onBackPressed() {
        if (mPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            mPager.currentItem = mPager.currentItem - 1
        }
    }

}
